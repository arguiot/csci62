#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , counter(new Counter())
{
    ui->setupUi(this);
    ui->label->setText("Number of Clicks: 0");
    ui->label_2->hide();
    // Increment counter on click
    connect(ui->pushButton, &QPushButton::clicked, this, &MainWindow::onButtonClicked);

}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::onButtonClicked()
{
    if (counter->getCount() >= 10) {
        ui->label_2->show();
        ui->label->hide();
        ui->pushButton->hide();
        return;
    }
    // Increment the counter
    counter->add();

    // Update the label text with the current count
    ui->label->setText("Number of Clicks: " + QString::number(counter->getCount()));
}
