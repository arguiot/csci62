// user.cpp
#include "user.h"
#include "post.h"
#include <algorithm>
#include <sstream>

User::User() : id_(-1), year_(-1), zip_(-1) {}

User::User(int id, const std::string &name, int year, int zip,
           const std::set<int> &friends)
    : id_(id), name_(name), year_(year), zip_(zip), friends_(friends) {}

void User::addFriend(int id) { friends_.insert(id); }

void User::deleteFriend(int id) { friends_.erase(id); }

void User::addPost(Post *post) { messages_.push_back(post); }

std::vector<Post *> User::getPosts() const { return messages_; }

std::string User::getPostsString(int howMany, bool showOnlyPublic) const {
  std::ostringstream oss;
  int count = 0;

  for (auto it = messages_.rbegin(); it != messages_.rend() && count < howMany;
       ++it) {
    Post *post = *it;
    IncomingPost *incomingPost = dynamic_cast<IncomingPost *>(post);

    if (showOnlyPublic && incomingPost != nullptr &&
        !incomingPost->getIsPublic()) {
      continue;
    }

    oss << post->toString() << "\n\n";
    ++count;
  }

  return oss.str();
}

int User::getId() const { return id_; }

const std::string &User::getName() const { return name_; }

int User::getYear() const { return year_; }

int User::getZip() const { return zip_; }

std::set<int> &User::getFriends() { return friends_; }
