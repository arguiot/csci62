// user.h
#ifndef USER_H
#define USER_H

#include "post.h"
#include <set>
#include <string>
#include <vector>

class User {
public:
  // Constructors
  User();
  User(int id, const std::string &name, int year, int zip,
       const std::set<int> &friends);

  // Friend management
  void addFriend(int id);
  void deleteFriend(int id);

  // Post management
  void addPost(Post *post);
  std::vector<Post *> getPosts() const;
  std::string getPostsString(int howMany, bool showOnlyPublic) const;

  // Getters
  int getId() const;
  const std::string &getName() const;
  int getYear() const;
  int getZip() const;
  std::set<int> &getFriends();

private:
  int id_;
  std::string name_;
  int year_;
  int zip_;
  std::set<int> friends_;
  std::vector<Post *> messages_;
};

#endif // USER_H
