// network.cpp
#include "network.h"
#include <fstream>
#include <iostream>
#include <queue>
#include <sstream>

Network::Network() {}

void Network::addUser(User *user) { users_.push_back(user); }

int Network::addConnection(const std::string &name1, const std::string &name2) {
  int id1 = getId(name1);
  int id2 = getId(name2);

  if (id1 == -1 || id2 == -1) {
    return -1; // User not found
  }

  users_[id1]->addFriend(id2);
  users_[id2]->addFriend(id1);
  return 0;
}

int Network::deleteConnection(const std::string &name1,
                              const std::string &name2) {
  int id1 = getId(name1);
  int id2 = getId(name2);

  if (id1 == -1 || id2 == -1) {
    return -1; // User not found
  }

  users_[id1]->deleteFriend(id2);
  users_[id2]->deleteFriend(id1);
  return 0;
}

int Network::getId(const std::string &name) const {
  for (int i = 0; i < static_cast<int>(users_.size()); ++i) {
    if (users_[i]->getName() == name) {
      return i;
    }
  }
  return -1; // User not found
}

int Network::readUsers(const char *filename) {
  std::ifstream file(filename);
  if (!file) {
    return -1; // Error opening file
  }

  int numUsers;
  file >> numUsers;

  for (int i = 0; i < numUsers; ++i) {
    int id, year, zip;
    std::string name;
    std::set<int> friendIds;

    file >> id;
    file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    std::string line;
    std::getline(file, line);
    name = line.substr(1); // Remove the tab character from the beginning

    std::getline(file, line);
    year = std::stoi(
        line.substr(1)); // Remove the tab character and convert to int

    std::getline(file, line);
    zip = std::stoi(
        line.substr(1)); // Remove the tab character and convert to int

    std::getline(file, line);
    std::istringstream iss(line.substr(1)); // Remove the tab character

    int friendId;
    while (iss >> friendId) {
      friendIds.insert(friendId);
    }

    User *user = new User(id, name, year, zip, friendIds);

    addUser(user);
  }

  file.close();
  return 0;
}

int Network::writeUsers(const char *filename) {
  std::ofstream file(filename);
  if (!file) {
    return -1; // Error opening file
  }

  file << users_.size() << std::endl;

  for (int i = 0; i < static_cast<int>(users_.size()); ++i) {
    User *user = users_[i];
    file << user->getId() << std::endl;
    file << "\t" << user->getName() << std::endl;
    file << "\t" << user->getYear() << std::endl;
    file << "\t" << user->getZip() << std::endl;
    file << "\t";

    const std::set<int> &friendIds = user->getFriends();
    for (int fId : friendIds) {
      file << fId << " ";
    }
    file << std::endl;
  }

  file.close();
  return 0;
}

int Network::numUsers() const { return static_cast<int>(users_.size()); }

User *Network::getUser(int id) {
  if (id < 0 || id >= static_cast<int>(users_.size())) {
    return nullptr; // Invalid id
  }
  return users_[id];
}

int *Network::getFriends(int id) {
  User *user = getUser(id);
  if (user == nullptr) {
    return nullptr; // Invalid id
  }

  const std::set<int> &friendIds = user->getFriends();
  int *friends = new int[friendIds.size() + 1];
  int i = 0;
  for (int fId : friendIds) {
    friends[i++] = fId;
  }
  friends[i] = -1; // Mark end of array
  return friends;
}

int *Network::getAllUsers() {
  int *allUsers = new int[users_.size() + 1];
  for (int i = 0; i < static_cast<int>(users_.size()); ++i) {
    allUsers[i] = i;
  }
  allUsers[users_.size()] = -1; // Mark end of array
  return allUsers;
}

std::vector<int> Network::shortestPath(int from, int to) {
  std::vector<std::vector<int>> graph(users_.size());
  buildGraph(graph);

  std::vector<int> prev(users_.size(), -1);
  bfs(from, to, graph, prev);

  std::vector<int> path;
  int current = to;
  while (current != -1) {
    path.push_back(current);
    current = prev[current];
  }
  std::reverse(path.begin(), path.end());

  return path;
}

void Network::buildGraph(std::vector<std::vector<int>> &graph) {
  for (int i = 0; i < static_cast<int>(users_.size()); ++i) {
    const std::set<int> &friendIds = users_[i]->getFriends();
    for (int fId : friendIds) {
      graph[i].push_back(fId);
    }
  }
}

void Network::bfs(int from, int to, std::vector<std::vector<int>> &graph,
                  std::vector<int> &prev) {
  std::queue<int> queue;
  std::vector<bool> visited(users_.size(), false);

  queue.push(from);
  visited[from] = true;

  while (!queue.empty()) {
    int current = queue.front();
    queue.pop();

    if (current == to) {
      break;
    }

    for (int neighbor : graph[current]) {
      if (!visited[neighbor]) {
        queue.push(neighbor);
        visited[neighbor] = true;
        prev[neighbor] = current;
      }
    }
  }
}

void Network::bfs(int from, std::vector<std::vector<int>> &graph,
                  std::vector<int> &prev, std::vector<int> &dist) {
  std::queue<int> queue;
  std::vector<bool> visited(users_.size(), false);

  queue.push(from);
  visited[from] = true;
  dist[from] = 0;

  while (!queue.empty()) {
    int current = queue.front();
    queue.pop();

    for (int neighbor : graph[current]) {
      if (!visited[neighbor]) {
        queue.push(neighbor);
        visited[neighbor] = true;
        prev[neighbor] = current;
        dist[neighbor] = dist[current] + 1;
      }
    }
  }
}

std::vector<int> Network::distanceUser(int from, int &to, int distance) {
  std::vector<std::vector<int>> graph(users_.size());
  buildGraph(graph);

  std::vector<int> prev(users_.size(), -1);
  std::vector<int> dist(users_.size(), -1);
  bfs(from, graph, prev, dist);

  for (int i = 0; i < static_cast<int>(users_.size()); ++i) {
    if (dist[i] == distance) {
      to = i;
      std::vector<int> path;
      int current = to;
      while (current != -1) {
        path.push_back(current);
        current = prev[current];
      }
      std::reverse(path.begin(), path.end());
      return path;
    }
  }

  to = -1;
  return std::vector<int>();
}

std::vector<int> Network::suggestFriends(int who, int &score) {
  std::vector<int> suggestions;
  score = 0;

  std::vector<bool> isFriend(users_.size(), false);
  const std::set<int> &friendIds = users_[who]->getFriends();
  for (int friendId : friendIds) {
    isFriend[friendId] = true;
  }

  for (int i = 0; i < static_cast<int>(users_.size()); ++i) {
    if (i == who || isFriend[i]) {
      continue;
    }

    int commonFriends = 0;
    const std::set<int> &candidateFriendIds = users_[i]->getFriends();
    for (int candidateFriendId : candidateFriendIds) {
      if (isFriend[candidateFriendId]) {
        ++commonFriends;
      }
    }

    if (commonFriends > score) {
      score = commonFriends;
      suggestions.clear();
      suggestions.push_back(i);
    } else if (commonFriends == score) {
      suggestions.push_back(i);
    }
  }

  return suggestions;
}

std::vector<std::vector<int>> Network::groups() {
  std::vector<std::vector<int>> components;
  std::vector<bool> visited(users_.size(), false);

  std::vector<std::vector<int>> graph(users_.size());
  buildGraph(graph);

  for (int i = 0; i < static_cast<int>(users_.size()); ++i) {
    if (!visited[i]) {
      std::vector<int> component;
      dfs(i, visited, component, graph);
      components.push_back(component);
    }
  }

  return components;
}

void Network::dfs(int node, std::vector<bool> &visited,
                  std::vector<int> &component,
                  const std::vector<std::vector<int>> &graph) {
  visited[node] = true;
  component.push_back(node);

  for (int neighbor : graph[node]) {
    if (!visited[neighbor]) {
      dfs(neighbor, visited, component, graph);
    }
  }
}

void Network::addPost(int ownerId, const std::string &message, int likes,
                      bool isIncoming, const std::string &authorName,
                      bool isPublic) {
  User *user = getUser(ownerId);
  if (user != nullptr) {
    Post *post;
    if (isIncoming) {
      post = new IncomingPost(nextPostId, ownerId, message, likes, isPublic,
                              authorName);
    } else {
      post = new Post(nextPostId, ownerId, message, likes);
    }
    user->addPost(post);
    nextPostId++; // Increment the global post ID
  }
}

std::string Network::getPostsString(int ownerId, int howMany,
                                    bool showOnlyPublic) {
  User *user = getUser(ownerId);
  if (user != nullptr) {
    return user->getPostsString(howMany, showOnlyPublic);
  }
  return "";
}

int Network::readPosts(const char *filename) {
  std::ifstream file(filename);
  if (!file) {
    return -1; // Error opening file
  }

  int numPosts;
  file >> numPosts;

  for (int i = 0; i < numPosts; ++i) {
    int messageId, ownerId, likes;
    std::string message, postType, author;

    file >> messageId;
    file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    std::getline(file, message);
    if (!message.empty() && message[0] == '\t') {
      message = message.substr(1); // Remove the tab character
    }

    file >> ownerId;
    file >> likes;

    file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::getline(file, postType);
    if (!postType.empty() && postType[0] == '\t') {
      postType = postType.substr(1); // Remove the tab character
    }

    if (!postType.empty()) {
      std::getline(file, author);
      if (!author.empty() && author[0] == '\t') {
        author = author.substr(1); // Remove the tab character
      }
    }

    User *user = getUser(ownerId);
    if (user != nullptr) {
      Post *post;
      if (postType.empty()) {
        post = new Post(messageId, ownerId, message, likes);
      } else {
        bool isPublic = (postType == "public");
        post = new IncomingPost(messageId, ownerId, message, likes, isPublic,
                                author);
      }
      user->addPost(post);
    }
  }

  file.close();
  return 0;
}

bool comparePostsByMessageId(const Post *a, const Post *b) {
  return a->getMessageId() < b->getMessageId();
}

int Network::writePosts(const char *filename) {
  std::ofstream file(filename);
  if (!file) {
    return -1; // Error opening file
  }

  int totalPosts = 0;
  for (User *user : users_) {
    totalPosts += user->getPosts().size();
  }

  file << totalPosts << std::endl;

  for (User *user : users_) {
    std::vector<Post *> userPosts = user->getPosts();
    for (Post *post : userPosts) {
      file << post->getMessageId() << std::endl;
      file << "\t" << post->getMessage() << std::endl;
      file << "\t" << post->getOwnerId() << std::endl;
      file << "\t" << post->getLikes() << std::endl;

      if (dynamic_cast<IncomingPost *>(post) != nullptr) {
        IncomingPost *incomingPost = dynamic_cast<IncomingPost *>(post);
        file << "\t" << (incomingPost->getIsPublic() ? "public" : "private")
             << std::endl;
        file << "\t" << incomingPost->getAuthor() << std::endl;
      } else {
        file << "\t" << std::endl;
        file << "\t" << std::endl;
      }
    }
  }

  file.close();
  return 0;
}
