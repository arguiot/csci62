// network.h
#ifndef NETWORK_H
#define NETWORK_H

#include "post.h"
#include "user.h"
#include <string>
#include <vector>

class Network {
public:
  Network();

  void addUser(User *user);
  int addConnection(const std::string &name1, const std::string &name2);
  int deleteConnection(const std::string &name1, const std::string &name2);
  int getId(const std::string &name) const;
  int readUsers(const char *filename);
  int writeUsers(const char *filename);
  int numUsers() const;
  User *getUser(int id);

  int *getFriends(int id);
  int *getAllUsers();
  std::vector<int> shortestPath(int from, int to);
  std::vector<int> distanceUser(int from, int &to, int distance);
  std::vector<int> suggestFriends(int who, int &score);
  std::vector<std::vector<int>> groups();

  void addPost(int ownerId, const std::string &message, int likes,
               bool isIncoming, const std::string &authorName, bool isPublic);
  std::string getPostsString(int ownerId, int howMany, bool showOnlyPublic);
  int readPosts(const char *filename);
  int writePosts(const char *filename);

private:
  std::vector<User *> users_;
  int nextPostId = 0; // Global post ID counter to pass test_readwritepost
  void buildGraph(std::vector<std::vector<int>> &graph);
  void bfs(int from, int to, std::vector<std::vector<int>> &graph,
           std::vector<int> &prev);
  void bfs(int from, std::vector<std::vector<int>> &graph,
           std::vector<int> &prev, std::vector<int> &dist);
  void dfs(int node, std::vector<bool> &visited, std::vector<int> &component,
           const std::vector<std::vector<int>> &graph);
};

#endif // NETWORK_H
