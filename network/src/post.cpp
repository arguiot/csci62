// post.cpp
#include "post.h"

// Post class implementation
Post::Post() : messageId_(0), ownerId_(0), likes_(0) {}

Post::Post(int messageId, int ownerId, const std::string &message, int likes)
    : messageId_(messageId), ownerId_(ownerId), message_(message),
      likes_(likes) {}

std::string Post::toString() const {
  return message_ + " (Liked by " + std::to_string(likes_) + " people)";
}

int Post::getMessageId() const { return messageId_; }

int Post::getOwnerId() const { return ownerId_; }

std::string Post::getMessage() const { return message_; }

int Post::getLikes() const { return likes_; }

std::string Post::getAuthor() const { return ""; }

bool Post::getIsPublic() const { return true; }

// IncomingPost class implementation
IncomingPost::IncomingPost() : Post(), author_(""), isPublic_(false) {}

IncomingPost::IncomingPost(int messageId, int ownerId,
                           const std::string &message, int likes, bool isPublic,
                           const std::string &author)
    : Post(messageId, ownerId, message, likes), author_(author),
      isPublic_(isPublic) {}

std::string IncomingPost::toString() const {
  std::string visibility = isPublic_ ? "" : " (private)";
  return author_ + " wrote" + visibility + ": " + Post::toString();
}

std::string IncomingPost::getAuthor() const { return author_; }

bool IncomingPost::getIsPublic() const { return isPublic_; }
