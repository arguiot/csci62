#include "network.h"
#include "user.h"
#include <iostream>
#include <sstream>
#include <string>

int main(int argc, char *argv[]) {
  if (argc < 3) {
    std::cerr << "Usage: " << argv[0] << " <users_file> <posts_file>"
              << std::endl;
    return 1;
  }

  Network network;
  if (network.readUsers(argv[1]) != 0) {
    std::cerr << "Error reading users file." << std::endl;
    return 1;
  }

  if (network.readPosts(argv[2]) != 0) {
    std::cerr << "Error reading posts file." << std::endl;
    return 1;
  }

  int option;
  std::string name1, name2;
  std::string filename;

  while (true) {
    std::cout << "Menu:\n"
              << "1. Add a user\n"
              << "2. Add friend connection\n"
              << "3. Delete friend connection\n"
              << "4. Print users\n"
              << "5. Print friends\n"
              << "6. Write to file\n"
              << "7. Shortest path\n"
              << "8. Distance from user\n"
              << "9. Suggest friends\n"
              << "10. Groups\n"
              << "11. View recent posts\n"
              << "Exit any other option\n"
              << "Enter input: ";
    std::cin >> option;

    switch (option) {
    case 1: {
      std::string name, lastName, birthYear, zipCode;
      std::cin >> name >> lastName >> birthYear >> zipCode;
      int birthYearInt = std::stoi(birthYear);
      int zipCodeInt = std::stoi(zipCode);
      User *user = new User(network.numUsers(), name + " " + lastName,
                            birthYearInt, zipCodeInt, std::set<int>());
      network.addUser(user);
      break;
    }
    case 2: {
      std::string line;
      std::getline(std::cin >> std::ws, line);

      std::istringstream iss(line);
      std::vector<std::string> names;
      std::string firstName, lastName;
      while (iss >> firstName >> lastName) {
        names.push_back(firstName + " " + lastName);
      }

      if (names.size() < 2) {
        std::cerr << "At least two names are required." << std::endl;
        break;
      }

      for (size_t i = 0; i < names.size() - 1; ++i) {
        for (size_t j = i + 1; j < names.size(); ++j) {
          std::string userId1 = names[i];
          std::string userId2 = names[j];
          network.addConnection(userId1, userId2);
        }
      }
      std::cout << "Friend connections added successfully." << std::endl;
      break;
    }
    case 3: {
      std::string line;
      std::getline(std::cin >> std::ws, line);

      std::istringstream iss(line);
      std::vector<std::string> names;
      std::string firstName, lastName;
      while (iss >> firstName >> lastName) {
        names.push_back(firstName + " " + lastName);
      }

      if (names.size() < 2) {
        std::cerr << "At least two names are required." << std::endl;
        break;
      }

      for (size_t i = 0; i < names.size() - 1; ++i) {
        for (size_t j = i + 1; j < names.size(); ++j) {
          std::string userId1 = names[i];
          std::string userId2 = names[j];
          network.deleteConnection(userId1, userId2);
        }
      }
      std::cout << "Friend connections removed successfully." << std::endl;
      break;
    }
    case 4: {
      int *ids = network.getAllUsers();
      int i = 0;
      while (ids[i] != -1) {
        User *user = network.getUser(ids[i]);
        std::cout << user->getId() << " " << user->getName() << " "
                  << user->getYear() << " " << user->getZip() << std::endl;
        i++;
      }
      break;
    }
    case 5: {
      std::cin >> name1 >> name2;
      int id = network.getId(name1 + " " + name2);
      if (id == -1) {
        std::cerr << "User not found." << std::endl;
      } else {
        int *friends = network.getFriends(id);
        int i = 0;
        while (friends[i] != -1) {
          User *user = network.getUser(friends[i]);
          std::cout << user->getId() << " " << user->getName() << " "
                    << user->getYear() << " " << user->getZip() << std::endl;
          i++;
        }
      }
      break;
    }
    case 6: {
      std::cin >> filename;
      if (network.writeUsers(filename.c_str()) != 0) {
        std::cerr << "Error writing to file." << std::endl;
      } else {
        std::cout << "Wrote " << network.numUsers() << " users to " << filename
                  << std::endl;
      }
      break;
    }
    case 7: {
      std::string line;
      std::getline(std::cin >> std::ws, line);

      std::istringstream iss(line);
      std::vector<std::string> names;
      std::string firstName, lastName;
      while (iss >> firstName >> lastName) {
        names.push_back(firstName + " " + lastName);
      }

      if (names.size() != 2) {
        std::cerr << "Exactly two names are required." << std::endl;
        break;
      }

      int id1 = network.getId(names[0]);
      int id2 = network.getId(names[1]);

      if (id1 == -1 || id2 == -1) {
        std::cerr << "User not found." << std::endl;
        break;
      }

      std::vector<int> path = network.shortestPath(id1, id2);

      if (path.empty()) {
        std::cout << "None" << std::endl;
      } else {
        std::cout << "Distance: " << path.size() - 1 << std::endl;
        for (size_t i = 0; i < path.size(); ++i) {
          std::cout << network.getUser(path[i])->getName();
          if (i < path.size() - 1) {
            std::cout << " -> ";
          }
        }
        std::cout << std::endl;
      }
      break;
    }
    case 8: {
      std::string line;
      std::getline(std::cin >> std::ws, line);

      std::istringstream iss(line);
      std::string firstName, lastName;
      int distance;
      iss >> firstName >> lastName >> distance;

      int fromId = network.getId(firstName + " " + lastName);

      if (fromId == -1) {
        std::cerr << "User not found." << std::endl;
        break;
      }

      int toId;
      std::vector<int> path = network.distanceUser(fromId, toId, distance);

      if (toId == -1) {
        std::cout << "No user found at the specified distance." << std::endl;
      } else {
        std::cout << network.getUser(toId)->getName() << ": ";
        for (size_t i = 0; i < path.size(); ++i) {
          std::cout << network.getUser(path[i])->getName();
          if (i < path.size() - 1) {
            std::cout << " -> ";
          }
        }
        std::cout << std::endl;
      }
      break;
    }
    case 9: {
      std::string line;
      std::getline(std::cin >> std::ws, line);

      std::istringstream iss(line);
      std::string firstName, lastName;
      iss >> firstName >> lastName;

      int userId = network.getId(firstName + " " + lastName);

      if (userId == -1) {
        std::cerr << "User not found." << std::endl;
        break;
      }

      int score;
      std::vector<int> suggestions = network.suggestFriends(userId, score);

      if (suggestions.empty()) {
        std::cout << "None" << std::endl;
      } else {
        std::cout << "The suggested friend(s) is/are:" << std::endl;
        for (int suggestionId : suggestions) {
          User *user = network.getUser(suggestionId);
          std::cout << user->getName() << " Score: " << score << std::endl;
        }
      }
      break;
    }
    case 10: {
      std::vector<std::vector<int>> components = network.groups();

      for (size_t i = 0; i < components.size(); ++i) {
        std::cout << "Set " << i + 1 << " => ";
        for (size_t j = 0; j < components[i].size(); ++j) {
          User *user = network.getUser(components[i][j]);
          std::cout << user->getName();
          if (j < components[i].size() - 1) {
            std::cout << ", ";
          }
        }
        std::cout << std::endl;
      }
      break;
    }
    case 11: {
      std::string line;
      std::getline(std::cin >> std::ws, line);

      std::istringstream iss(line);
      std::string firstName, lastName;
      int howMany;
      iss >> firstName >> lastName >> howMany;

      int userId = network.getId(firstName + " " + lastName);

      if (userId == -1) {
        std::cerr << "User not found." << std::endl;
        break;
      }

      std::string postsString = network.getPostsString(userId, howMany, false);

      if (postsString.empty()) {
        std::cout << "No posts found." << std::endl;
      } else {
        std::cout << postsString << std::endl;
      }
      break;
    }
    default:
      return 0;
    }
  }

  return 0;
}
