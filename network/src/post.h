// post.h
#ifndef POST_H
#define POST_H

#include <string>

class Post {
public:
  Post();
  Post(int messageId, int ownerId, const std::string &message, int likes);
  virtual ~Post() = default;

  virtual std::string toString() const;
  int getMessageId() const;
  int getOwnerId() const;
  std::string getMessage() const;
  int getLikes() const;
  virtual std::string getAuthor() const;
  virtual bool getIsPublic() const;

private:
  int messageId_;
  int ownerId_;
  std::string message_;
  int likes_;
};

class IncomingPost : public Post {
public:
  IncomingPost();
  IncomingPost(int messageId, int ownerId, const std::string &message,
               int likes, bool isPublic, const std::string &author);

  std::string toString() const override;
  std::string getAuthor() const override;
  bool getIsPublic() const override;

private:
  std::string author_;
  bool isPublic_;
};

#endif // POST_H
