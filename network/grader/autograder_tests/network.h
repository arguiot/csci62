// network.h
#ifndef NETWORK_H
#define NETWORK_H

#include "user.h"
#include <string>
#include <vector>

class Network {
public:
  // default constructor
  // pre: none
  // post: creates an empty Network object
  Network();

  // addUser
  // pre: user is a valid pointer to a User object
  // post: adds the given user to the network
  void addUser(User *user);

  // addConnection
  // pre: name1 and name2 are valid user names in the network
  // post: adds a connection between the users with the given names
  //       returns 0 if successful, -1 if either user is not found
  int addConnection(const std::string &name1, const std::string &name2);

  // deleteConnection
  // pre: name1 and name2 are valid user names in the network
  // post: removes the connection between the users with the given names
  //       returns 0 if successful, -1 if either user is not found or no
  //       connection exists
  int deleteConnection(const std::string &name1, const std::string &name2);

  // getId
  // pre: name is a valid user name in the network
  // post: returns the id of the user with the given name, or -1 if not found
  int getId(const std::string &name) const;

  // readUsers
  // pre: filename is a valid file path
  // post: reads user data from the given file and adds the users to the network
  //       returns the number of users read, or -1 if an error occurs
  int readUsers(const char *filename);

  // writeUsers
  // pre: filename is a valid file path
  // post: writes the user data to the given file
  //       returns the number of users written, or -1 if an error occurs
  int writeUsers(const char *filename);

  // numUsers
  // pre: none
  // post: returns the number of users in the network
  int numUsers() const;

  // getUser
  // pre: id is a valid user id in the network
  // post: returns a pointer to the User object with the given id, or nullptr if
  // not found
  User *getUser(int id);

  // getFriends
  // pre: id is a valid user id in the network
  // post: returns a pointer to an array of friend ids for the user with the
  // given id
  //       the array is dynamically allocated and must be deleted by the caller
  int *getFriends(int id);

  // getAllUsers
  // pre: none
  // post: returns a pointer to an array of all user ids in the network
  //       the array is dynamically allocated and must be deleted by the caller
  int *getAllUsers();

private:
  std::vector<User *> users_;
};

#endif // NETWORK_H
