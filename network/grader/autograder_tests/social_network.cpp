#include "network.h"
#include "user.h"
#include <iostream>
#include <sstream>
#include <string>

int main(int argc, char *argv[]) {
  if (argc < 2) {
    std::cerr << "Usage: " << argv[0] << " <input_file>" << std::endl;
    return 1;
  }

  Network network;
  if (network.readUsers(argv[1]) != 0) {
    std::cerr << "Error reading input file." << std::endl;
    return 1;
  }

  int option;
  std::string name1, name2;
  std::string filename;

  while (true) {
    std::cout << "Menu:\n"
              << "1. Add a user\n"
              << "2. Add friend connection\n"
              << "3. Delete friend connection\n"
              << "4. Print users\n"
              << "5. Print friends\n"
              << "6. Write to file\n"
              << "Exit any other option\n"
              << "Enter input: ";
    std::cin >> option;

    switch (option) {
    case 1: {
      std::string name, lastName, birthYear, zipCode;
      std::cin >> name >> lastName >> birthYear >> zipCode;
      int birthYearInt = std::stoi(birthYear);
      int zipCodeInt = std::stoi(zipCode);
      User *user = new User(network.numUsers(), name + " " + lastName,
                            birthYearInt, zipCodeInt, std::set<int>{});
      network.addUser(user);
      break;
    }
    case 2: {
      std::string line;
      std::getline(std::cin >> std::ws, line);

      std::istringstream iss(line);
      std::vector<std::string> names;
      std::string firstName, lastName;
      while (iss >> firstName >> lastName) {
        names.push_back(firstName + " " + lastName);
      }

      if (names.size() < 2) {
        std::cerr << "At least two names are required." << std::endl;
        break;
      }

      for (size_t i = 0; i < names.size() - 1; ++i) {
        for (size_t j = i + 1; j < names.size(); ++j) {
          std::string userId1 = names[i];
          std::string userId2 = names[j];
          network.addConnection(userId1, userId2);
        }
      }
      std::cout << "Friend connections added successfully." << std::endl;
      break;
    }
    case 3: {
      std::string line;
      std::getline(std::cin >> std::ws, line);

      std::istringstream iss(line);
      std::vector<std::string> names;
      std::string firstName, lastName;
      while (iss >> firstName >> lastName) {
        names.push_back(firstName + " " + lastName);
      }

      if (names.size() < 2) {
        std::cerr << "At least two names are required." << std::endl;
        break;
      }

      for (size_t i = 0; i < names.size() - 1; ++i) {
        for (size_t j = i + 1; j < names.size(); ++j) {
          std::string userId1 = names[i];
          std::string userId2 = names[j];
          network.deleteConnection(userId1, userId2);
        }
      }
      std::cout << "Friend connections removed successfully." << std::endl;
      break;
    }
    case 4: {
      int *ids = network.getAllUsers();
      int i = 0;
      while (ids[i] != -1) {
        User *user = network.getUser(ids[i]);
        std::cout << user->getId() << " " << user->getName() << " "
                  << user->getYear() << " " << user->getZip() << std::endl;
        i++;
      }
      break;
    }
    case 5: {
      std::cin >> name1 >> name2;
      int id = network.getId(name1 + " " + name2);
      if (id == -1) {
        std::cerr << "User not found." << std::endl;
      } else {
        int *friends = network.getFriends(id);
        int i = 0;
        while (friends[i] != -1) {
          User *user = network.getUser(friends[i]);
          std::cout << user->getId() << " " << user->getName() << " "
                    << user->getYear() << " " << user->getZip() << std::endl;
          i++;
        }
      }
      break;
    }
    case 6: {
      std::cin >> filename;
      if (network.writeUsers(filename.c_str()) != 0) {
        std::cerr << "Error writing to file." << std::endl;
      } else {
        std::cout << "Wrote " << network.numUsers() << " users to " << filename
                  << std::endl;
      }
      break;
    }
    default:
      return 0;
    }
  }

  return 0;
}
