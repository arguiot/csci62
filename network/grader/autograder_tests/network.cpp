// network.cpp
#include "network.h"
#include <fstream>
#include <iostream>
#include <sstream>

Network::Network() {}

void Network::addUser(User *user) { users_.push_back(user); }

int Network::addConnection(const std::string &name1, const std::string &name2) {
  int id1 = getId(name1);
  int id2 = getId(name2);

  if (id1 == -1 || id2 == -1) {
    return -1; // User not found
  }

  users_[id1]->addFriend(id2);
  users_[id2]->addFriend(id1);
  return 0;
}

int Network::deleteConnection(const std::string &name1,
                              const std::string &name2) {
  int id1 = getId(name1);
  int id2 = getId(name2);

  if (id1 == -1 || id2 == -1) {
    return -1; // User not found
  }

  users_[id1]->deleteFriend(id2);
  users_[id2]->deleteFriend(id1);
  return 0;
}

int Network::getId(const std::string &name) const {
  for (int i = 0; i < static_cast<int>(users_.size()); ++i) {
    if (users_[i]->getName() == name) {
      return i;
    }
  }
  return -1; // User not found
}

int Network::readUsers(const char *filename) {
  std::ifstream file(filename);
  if (!file) {
    return -1; // Error opening file
  }

  int numUsers;
  file >> numUsers;

  for (int i = 0; i < numUsers; ++i) {
    int id, year, zip;
    std::string name;
    std::set<int> friendIds;

    file >> id;
    file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    std::string line;
    std::getline(file, line);
    name = line.substr(1); // Remove the tab character from the beginning

    std::getline(file, line);
    year = std::stoi(
        line.substr(1)); // Remove the tab character and convert to int

    std::getline(file, line);
    zip = std::stoi(
        line.substr(1)); // Remove the tab character and convert to int

    std::getline(file, line);
    std::istringstream iss(line.substr(1)); // Remove the tab character

    int friendId;
    while (iss >> friendId) {
      friendIds.insert(friendId);
    }

    User *user = new User(id, name, year, zip, friendIds);

    addUser(user);
  }

  file.close();
  return 0;
}

int Network::writeUsers(const char *filename) {
  std::ofstream file(filename);
  if (!file) {
    return -1; // Error opening file
  }

  file << users_.size() << std::endl;

  for (int i = 0; i < static_cast<int>(users_.size()); ++i) {
    User *user = users_[i];
    file << user->getId() << std::endl;
    file << "\t" << user->getName() << std::endl;
    file << "\t" << user->getYear() << std::endl;
    file << "\t" << user->getZip() << std::endl;
    file << "\t";

    const std::set<int> &friendIds = user->getFriends();
    for (int fId : friendIds) {
      file << fId << " ";
    }
    file << std::endl;
  }

  file.close();
  return 0;
}

int Network::numUsers() const { return static_cast<int>(users_.size()); }

User *Network::getUser(int id) {
  if (id < 0 || id >= static_cast<int>(users_.size())) {
    return nullptr; // Invalid id
  }
  return users_[id];
}

int *Network::getFriends(int id) {
  User *user = getUser(id);
  if (user == nullptr) {
    return nullptr; // Invalid id
  }

  const std::set<int> &friendIds = user->getFriends();
  int *friends = new int[friendIds.size() + 1];
  int i = 0;
  for (int fId : friendIds) {
    friends[i++] = fId;
  }
  friends[i] = -1; // Mark end of array
  return friends;
}

int *Network::getAllUsers() {
  int *allUsers = new int[users_.size() + 1];
  for (int i = 0; i < static_cast<int>(users_.size()); ++i) {
    allUsers[i] = i;
  }
  allUsers[users_.size()] = -1; // Mark end of array
  return allUsers;
}
