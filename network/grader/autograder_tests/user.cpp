// user.cpp
#include "user.h"

User::User() : id_(-1), year_(-1), zip_(-1) {}

User::User(int id, const std::string &name, int year, int zip,
           const std::set<int> &friends)
    : id_(id), name_(name), year_(year), zip_(zip), friends_(friends) {}

void User::addFriend(int id) { friends_.insert(id); }

void User::deleteFriend(int id) { friends_.erase(id); }

int User::getId() const { return id_; }

const std::string &User::getName() const { return name_; }

int User::getYear() const { return year_; }

int User::getZip() const { return zip_; }

std::set<int> &User::getFriends() { return friends_; }
