for file in test_*.cpp; do
    g++ -std=c++11 -o "${file%.cpp}" $file network.cpp user.cpp -I . && ./"${file%.cpp}"
done
g++ -std=c++11 -o social_network main.cpp network.cpp user.cpp -I . && python3 command_line_tests.py
