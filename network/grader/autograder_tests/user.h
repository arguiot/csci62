// user.h
#ifndef USER_H
#define USER_H

#include <set>
#include <string>

class User {
public:
  // Constructors

  // default constructor
  // pre: none
  // post: creates a User object with default values
  User();

  // parameterized constructor
  // pre: id >= 0, year >= 0, zip >= 0
  // post: creates a User object with the given values
  User(int id, const std::string &name, int year, int zip,
       const std::set<int> &friends);

  // Friend management

  // addFriend
  // pre: id >= 0
  // post: adds the given id to the user's friends set
  void addFriend(int id);

  // deleteFriend
  // pre: id >= 0
  // post: removes the given id from the user's friends set
  void deleteFriend(int id);

  // Getters

  // getId
  // pre: none
  // post: returns the user's id
  int getId() const;

  // getName
  // pre: none
  // post: returns the user's name
  const std::string &getName() const;

  // getYear
  // pre: none
  // post: returns the user's year
  int getYear() const;

  // getZip
  // pre: none
  // post: returns the user's zip code
  int getZip() const;

  // getFriends
  // pre: none
  // post: returns a reference to the user's friends set
  std::set<int> &getFriends();

private:
  int id_;
  std::string name_;
  int year_;
  int zip_;
  std::set<int> friends_;
};

#endif // USER_H
